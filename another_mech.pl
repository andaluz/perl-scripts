use strict;
$|++;

#use WWW::Mechanize;
#use File::Basename;
#
#my $m = WWW::Mechanize->new;
#
##$m->get("http://www.despair.com/indem.html");
##$m->get("http://dev.illoye.com");
#$m->get("http://dev.illoye.com");
#
#my @top_links = @{$m->links};
#
#for my $top_link_num (0..$#top_links) {
#        next unless $top_links[$top_link_num][0] =~ /^http:/;
#
#        $m->follow_link( n=>$top_link_num ) or die
#        "can't follow $top_link_num";
#
#        print $m->uri, "\n";
#        #for my $image (grep m{^http://store4}, map
#        #        $_->[0], @{$m->links}) {
#        #        my $local = basename $image;
#        #        print " $image...", $m->mirror($image, $local)->message, "\n"
#        #}
#
#        $m->back or die "can't go back";
#}


#-----------------------------------------------
# De code hieronder werkt, maar moet voorzichtig
# stap voor stap geanaliseerd worden, om te
# begrijpen hoe het een en ander werkt.
# De documentatie is ietwat beperkt en lijkt niet
# up-to-date te zijn.
#-----------------------------------------------
use WWW::Mechanize;
use Data::Dump qw( dump );

my $m = WWW::Mechanize->new;
my $url = 'http://dev.illoye.com';

eval {
        $m->get($url);
}; warn $@ if $@;

#dump $m;
#unless($m->success()){ print "Failed to request page...\n";}

#print $m->content(format => q(text));
print $m->content();

#$m->success or print "Error!!!\n";

# if/else statement moet met curly brace gebruikt worden,
# anders krijg je compile-error
if($m->success()){
        print "\nSuccessfully requested page\n\n";
        my $text = $m->dump_text();
        print "$text";
} else {
        print "\nFailed to request html page\n\n";
}


