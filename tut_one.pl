#!perl -w
# -w means add compile warnings
# -T means Taint, this will check for security from user-input
# use strict; check on correctness of declaring variables and subroutines

# A scalar is a piece of data, it could be an integer,
# a string, a block of binary data or even a webpage-content
#

#=BEGIN naamvancomment
#dit is een manier om
#commentaar toe te voegen...
#=CUT

#$doc=<<"EOF"
#Dit is een manier om een groot stuk
#tekst aan een variabele toe te kennen.
#We kunnen het gebruiken om teksten te
#parsen en te verwerken.
#EOF

# Dit is een simpel print statement
print "Yo wazup, this perl!!\n";
print "This is a Perl tutorial script.\n\n";
print "Filename: ".__FILE__."\n\n";
print "This text is printed on line: ".__LINE__."\n\n";
print "The current package is: ".__PACKAGE__."\n\n";

$age = 25;             # An integer assignment
$name = "John Paul";   # A string 
$salary = 1445.50;     # A floating point

print "age = $age\n";
print "name = $name\n";
print "salary = $salary\n";
print "----------------------------\n";

foo();
string_test();

print "----------------------------\n";

sub string_test {
        print "This is a "."concatenated string :)\n";
        print "...and this is
        a multilined
        string.\n";
}

sub foo {
        my $age = 15;             # An integer assignment
        my $name = "Nordin Wannabe";   # A string 
        my $salary = "te weinig!";     # A floating point

        print "age = $age\n";
        print "name = $name\n";
        print "salary = $salary\n";
}





$smile  = v9786;
$foo    = v102.111.111;
$martin = v77.97.114.116.105.110; 

print "smile = $smile\n";
print "foo = $foo\n";
print "martin = $martin\n\n";


# Laten we spelen met arrays!
@simple_array = (1, 2, 3, 4, 6);
@scalar_array = (
        'aap',
        'kees', 
        'miep', 
        'boom', 
        'muis');

print_array();
sub print_array() {
        foreach $x (@scalar_array) {
                print "element: $x\n";
                print "---------------------\n";
        }
        push(@scalar_array, 'noot');
        print "element: @scalar_array[5]\n";
        print "---------------------\n";
}

# Other array processing are: pop, shift and unshift

print "---------------------\n";
print "---------------------\n";

@another_array = qw/Yo wazup peeps, Perl is cool/;
foreach $y (@another_array) {
        print "$y\n";
        print "---------------------\n";
}

print "---------------------\n";
print "---------------------\n";

@var_10 = (1..10);
@var_20 = (10..20);
@var_abc = (a..z);

print "@var_10\n";   # Prints number from 1 to 10
print "@var_20\n";   # Prints number from 10 to 20
print "@var_abc\n";  # Prints number from a to z


@days = qw/Mon Tue Wed Thu Fri Sat Sun/;
@weekdays = @days[3,4,5]; # @days[3..5] has same result
print "@weekdays\n";


# split a string in words:
#define Strings
$var_string = "Rain-Drops-On-Roses-And-Whiskers-On-Kittens";
$var_names = "Larry,David,Roger,Ken,Michael,Tom";

# transform above strings into arrays.
@string = split('-', $var_string);
@names  = split(',', $var_names);

print "$string[3]\n";  # This will print Roses
print "$names[4]\n";   # This will print Michael


print "\n\n";
print "---------------------\n";
print "---------------------\n";

#%data = ('John Paul', 45, 'Lisa', 30, 'Kumar', 40); # <-- same result as the next line
%data = ('John Paul' => 45, 'Lisa' => 30, 'Kumar' => 40);

print "\$data{'John Paul'} = $data{'John Paul'}\n";
print "\$data{'Lisa'} = $data{'Lisa'}\n";
print "\$data{'Kumar'} = $data{'Kumar'}\n";
print "---------------------\n";

print "print array directly: @data\n";
print "---------------------\n";


print "---------------------\n";
print "---------------------\n";

# Date/Time
# localtime()
#sec,     # seconds of minutes from 0 to 61
#min,     # minutes of hour from 0 to 59
#hour,    # hours of day from 0 to 24
#mday,    # day of month from 1 to 31
#mon,     # month of year from 0 to 11
#year,    # year since 1900
#wday,    # days since sunday
#yday,    # days since January 1st
#isdst    # hours of daylight savings time
@months = qw( Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec );
@days = qw(Sun Mon Tue Wed Thu Fri Sat Sun);

($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime();
print "$mday $months[$mon] $days[$wday]\n";

print "---------------------\n";

$datestring = localtime();
print "Local date and time: $datestring\n";

print "---------------------\n";

$datestring = gmtime();
print "UTC-time: $datestring\n";

print "---------------------\n";

$epoc = time();
print "Number of seconds since Jan 1, 1970:  $epoc\n";


# subroutines (or functions)
# Note that using '&' to call a sub, will pass @_ array
# if not, than @_ is not passed.
# NAME(LIST); # & is optional with parentheses.
# NAME LIST;  # Parentheses optional if predeclared/imported.
# &NAME(LIST); # Circumvent prototypes.
# &NAME;      # Makes current @_ visible to called subroutine.
#
# Function definition
sub Average{
        # get total number of arguments passed.
        $n = scalar(@_);
        $sum = 0;

        foreach $item (@_){
                $sum += $item;
        }
        $average = $sum / $n;

        print "Average for the given numbers : $average\n";
}

print "---------------------\n";

# Function call
&Average(10, 20, 30);


sub PrintList{
        my @list = @_;
        print "Given list is @list\n";
}
$a = 10;
@b = (1, 2, 3, 4);

print "---------------------\n\n";

# Function call with list parameter
&PrintList($a, @b);

print "---------------------\n\n";

sub PrintHash{
        my (%hash) = @_;

        foreach $item (%hash){
                print "Item : $item\n";
        }
}
%hash = ('name' => 'Tom', 'age' => 19);

# Function call with hash parameter
&PrintHash(%hash);

print "---------------------\n\n";

sub Average{
        # get total number of arguments passed.
        $n = scalar(@_);
        $sum = 0;

        foreach $item (@_){
                $sum += $item;
        }
        $average = $sum / $n;

        return $average;
}

# Function call
$num = &Average(10, 20, 30);
print "Average for the given numbers : $num\n";

print "---------------------\n\n";



# another example [format]
format EMPLOYEE =
===================================
@<<<<<<<<<<<<<<<<<<<<<< @<< 
$name $age
@#####.##
$salary
===================================
.

format EMPLOYEE_TOP =
===================================
Name                    Age
===================================
.

select(STDOUT);
$~ = EMPLOYEE;
$^ = EMPLOYEE_TOP;

@n = ("Ali", "Raza", "Jaffer");
@a  = (20,30, 40);
@s = (2000.00, 2500.00, 4000.000);

$i = 0;
foreach (@n){
        $name = $_;
        $age = $a[$i];
        $salary = $s[$i++];
        write;
}

print "\n\n";

# open a file for reading 
open(FILE, "<randm.txt") || die "Can't open file randm.txt, $!";
while(<FILE>) {
        print "$_";
}

print "\n\n";

print "What is your name?\n";
$name = <STDIN>;
print "Hello $name\n";

